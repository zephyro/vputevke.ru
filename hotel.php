<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="main__container">
                    <div class="main__nav">

                        <!-- Sidenav -->
                        <div class="main__nav">
                            <?php include('inc/sidenav.inc.php') ?>
                        </div>
                        <!-- -->

                    </div>
                    <section class="main__content">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Новости</a></li>
                            <li><span>Пляжный отдых</span></li>
                        </ul>

                        <div class="hotel_heading">
                            <h1>Hotel Terminus</h1>
                            <span class="hotel_heading_star"><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                            <div class="hotel_heading_like"><i class="fa fa-thumbs-o-up"></i> <span><strong>4,78</strong>/<sub>5</sub></span></div>
                            <div class="hotel_heading_country">Австрия, Вена</div>
                        </div>

                        <div class="main__wrap">

                            <div class="hotel_slider">

                                <div class="hotel_slider_body swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/01.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/02.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/03.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/04.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/05.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/06.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="hotel_slider_item">
                                                <img src="images/hotel/07.jpg" class="hotel_slider_image" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>

                                </div>

                                <ul class="hotel_slider_thumbs">
                                   <li class="active">
                                        <a href="#" data-target="0">
                                            <img src="images/hotel/01.jpg" class="img-fluid" alt="">
                                        </a>
                                   </li>
                                    <li>
                                        <a href="#" data-target="1">
                                            <img src="images/hotel/02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="2">
                                            <img src="images/hotel/03.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="3">
                                            <img src="images/hotel/04.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="4">
                                            <img src="images/hotel/05.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="5">
                                            <img src="images/hotel/06.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-target="6">
                                            <img src="images/hotel/07.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                </ul>

                            </div>

                            <div class="hotel_content">
                                <h2>Об отеле</h2>
                                <p>Совсем недалеко от центра Вены, в тихом переулке района Нойбау, прямо на торговой улице Марияхильферштрассе расположен небольшой Terminus Hotel, отличающийся достаточно комфортными условиями, доброжелательностью персонала, говорящего на нескольких языках, и совсем невысокими, по венским меркам, ценами. Такое сочетание качеств привлекает туристов из самых разных стран мира, стремящихся при выборе отеля одновременно к удобству и экономичности.</p>
                                <p>Особенностью этой гостиницы, отмеченной двумя звездами, стал индивидуальный подход к оформлению каждого из сорока пяти номеров – здесь нет присущей многим отелям однотипности, когда, случайно зайдя в чужие апартаменты, даже сразу не понимаешь, что ошибся. Каждая комната имеет свои особенности, поэтому сам выбор номера уже становится интереснейшим занятием. Что же касается удобств, то их наличие не зависит от стиля интерьера – каждый посетитель получает апартаменты с санузлом, кондиционером, кабельным телевидением и современным телевизором, телефоном и радио</p>
                                <p>Утро каждого человека начинается с завтрака, и Terminus Hotel, с данной точки зрения, не является исключением. Ежедневно здесь сервируется сытная утренняя трапеза, состоящая из свежей традиционной австрийской выпечки, чая и кофе, горячих и холодных бутербродов, сырной и колбасной нарезки, сосисок. В вестибюле в автомате продаются печенье, батончики и ароматный кофе. Здесь же стоят столы для бильярда, кроме того, в любом общественном месте гостиницы действует бесплатный Wi-Fi.</p>
                                <p>Посетители отеля, приезжающие в столицу Австрии для встречи с деловыми партнерами, также не остались без внимания. К их услугам – круглосуточно работающий бизнес-центр, а персонал отеля способен существенно помочь в организации и проведении деловой встречи.</p>
                                <p>Недалеко от гостиницы находится ресторан Hanil Running Sushi, в котором предлагаются как хорошо знакомые любому европейцу блюда, так и традиционные австрийские. И конечно же – домашняя выпечка! Здесь довольно сложно найти кафе или ресторан, в меню которого не было бы этого пункта, ведь многие туристы приезжают в Вену, в том числе, и чтобы попробовать знаменитые венские булочки. Совсем недалеко от отеля находится живописный Naschmarkt – венский продуктовый рынок на открытой площади, насчитывающий около ста двадцати торговых мест и обустроенных в его павильонах ресторанов.</p>
                                <p>Любители постоянно узнавать что-то новое и знакомиться с достопримечательностями, пешком или с помощью общественного транспорта (остановка расположена рядом с гостиницей) довольно быстро добираются до центра города, где имеется немало интересных мест. Одним из них вполне можно назвать Венский музей мебели, в котором посетители окунаются в самые разные эпохи и стили – все экспонаты, коих в музее представлено около ста шестидесяти пяти тысяч, собирались в течение трехсот лет, и стали демонстрацией отношения к дизайну интерьера разных слоев общества в разные времена. Также в нескольких минутах ходьбы от отеля расположен Josefstadt – один из старейших действующих театров города, который ежегодно дает около семисот представлений.</p>
                            </div>

                            <div class="hotel_service">
                                <ul>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/infrastructure_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Интернет</h4>
                                            <ul>
                                                <li>Wi-Fi</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/serv_hotel_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Общие услуги</h4>
                                            <ul>
                                                <li>Круглосуточная стойка регистрации</li>
                                                <li>Лифт</li>
                                                <li>Экспресс-регистрация</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/infrastructure_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Парковка</h4>
                                            <ul>
                                                <li>Общественная</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/rooms_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Удобства в номерах</h4>
                                            <ul>
                                                <li>Сейф</li>
                                                <li>Отопление</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/infrastructure_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Специальные номера</h4>
                                            <ul>
                                                <li>Для некурящих</li>
                                                <li>Семейные</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/infrastructure_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Развлечения</h4>
                                            <ul>
                                                <li>Бильярд</li>
                                                <li>Дартс</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel_service_item">
                                            <div class="hotel_service_icon">
                                                <img src="img/infrastructure_icon.jpg" class="img-fluid" alt="">
                                            </div>
                                            <h4>Курение</h4>
                                            <ul>
                                                <li>Места для курения</li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="hotel_map">
                                <div class="hotel_map_wrap">
                                    <iframe src="https://www.google.com/maps/embed/v1/view?key=AIzaSyBVec-hDitxTpRsV09_QmYjqCjk-tx4k0M&amp;center=48.19999,16.35944&amp;zoom=20&amp;maptype=satellite" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                                </div>
                            </div>

                        </div>

                    </section>
                    <aside class="main__sidebar">

                        <!-- Countries -->
                        <?php include('inc/countries.inc.php') ?>
                        <!-- -->

                    </aside>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
