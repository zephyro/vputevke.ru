<div class="countries">
    <div class="countries__heading">Страны</div>
    <div class="countries__list">
        <a class="countries__item flag_ab" href="#">
            <span class="countries__name">Абхазия</span>
            <span class="countries__price">от 6291</span>
        </a>
        <a class="countries__item flag_at" href="#">
            <span class="countries__name">Австрия</span>
            <span class="countries__price">от 16775</span>
        </a>
        <a class="countries__item flag_az" href="#">
            <span class="countries__name">Азербайджан</span>
            <span class="countries__price">от 11545</span>
        </a>
        <a class="countries__item flag_ad" href="#">
            <span class="countries__name">Андорра</span>
            <span class="countries__price">от 22871</span>
        </a>
        <a class="countries__item flag_am" href="#">
            <span class="countries__name">Армения</span>
            <span class="countries__price">от 14825</span>
        </a>
        <a class="countries__item flag_bh" href="#">
            <span class="countries__name">Бахрейн</span>
            <span class="countries__price">от 14825</span>
        </a>
        <a class="countries__item flag_by" href="#">
            <span class="countries__name">Беларусь</span>
            <span class="countries__price">от 14825</span>
        </a>
        <a class="countries__item flag_be" href="#">
            <span class="countries__name">Бельгия</span>
            <span class="countries__price">от 14825</span>
        </a>
        <a class="countries__item flag_bg" href="#">
            <span class="countries__name">Болгария</span>
            <span class="countries__price">от 14825</span>
        </a>
        <a class="countries__item flag_gb" href="#">
            <span class="countries__name">Великобритания</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_hy" href="#">
            <span class="countries__name">Венгрия</span>
            <span class="countries__price">от 18215</span>
        </a>
        <a class="countries__item flag_vn" href="#">
            <span class="countries__name">Вьетнам</span>
            <span class="countries__price">от 18215</span>
        </a>
        <a class="countries__item flag_gr" href="#">
            <span class="countries__name">Греция</span>
            <span class="countries__price">от 6291</span>
        </a>
        <a class="countries__item flag_ge" href="#">
            <span class="countries__name">Грузия</span>
            <span class="countries__price">от 16775</span>
        </a>
        <a class="countries__item flag_dk" href="#">
            <span class="countries__name">Дания</span>
            <span class="countries__price">от 11545</span>
        </a>
        <a class="countries__item flag_il" href="#">
            <span class="countries__name">Израиль</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_io" href="#">
            <span class="countries__name">Иордания</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_ie" href="#">
            <span class="countries__name">Ирландия</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_es" href="#">
            <span class="countries__name">Испания</span>
            <span class="countries__price">от 27543</span>
        </a>

        <a class="countries__item flag_it" href="#">
            <span class="countries__name">Италия</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_qa" href="#">
            <span class="countries__name">Катар</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_cy" href="#">
            <span class="countries__name">Кипр</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_cn" href="#">
            <span class="countries__name">Китай</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_lv" href="#">
            <span class="countries__name">Латвия</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_lt" href="#">
            <span class="countries__name">Литва</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_mv" href="#">
            <span class="countries__name">Мальдивы</span>
            <span class="countries__price">от 27543</span>
        </a>
        <a class="countries__item flag_mt" href="#">
            <span class="countries__name">Мальта</span>
            <span class="countries__price">от 27543</span>
        </a>
    </div>
</div>