<div class="filters">
    <span class="filters__close filter-toggle"></span>
    <div class="filters__heading">Фильтры</div>
    <div class="filters__content">
        <div class="filters__content_wrap">
            <div class="filters__box">
                <div class="filters__box_title">Курорты</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Мармарис</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Бодрум</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Алания</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Кемер</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Сиде</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="place" value="">
                                <span>Стамбул</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Бюджет</div>
                <div class="filters__box_content">
                    <div class="filters__price">
                        <input type="text" class="filters__price_slider" value="" />
                    </div>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Класс отеля</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span>Любой</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span class="input_star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span class="input_star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span class="input_star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span class="input_star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span class="input_star">
                                        <i class="fa fa-star"></i>
                                    </span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Питание</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="" checked>
                                <span>Любое</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>UAI - Ультра всё включено</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>AI - Всё включено</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>BB - Завтрак</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>FB - Завтрак, обед, ужин</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>HB - Завтрак+ужин</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="food" value="">
                                <span>RO - Без питания</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Сервис</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="service" value="">
                                <span>Отдых с детьми</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="service" value="">
                                <span>Молодежный отдых</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="service" value="">
                                <span>Интренет</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="service" value="">
                                <span>Кондиционер</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="service" value="">
                                <span>Собственный пляж</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Тип пляжа</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span>Песчанный</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="beach" value="">
                                <span>Галечный</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filters__box">
                <div class="filters__box_title">Туроператоры</div>
                <div class="filters__box_content">
                    <ul class="filters__box_list">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="operator" value="">
                                <span>Annex</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="operator" value="">
                                <span>Sunmar</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="operator" value="">
                                <span>Pegas Touristik</span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>