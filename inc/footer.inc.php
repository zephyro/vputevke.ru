<footer class="footer">
    <div class="footer__container">
        <div class="footer__row">
            <div class="footer__logo">
                <a href="/">
                    <img src="img/logo.png" alt="" class="img-fluid">
                </a>
            </div>

            <div class="footer__address">
                ООО «ВПУТЕВКЕ» 2018<br/>
                191119 г. Санкт-Петербург,
                ул. Достоевского, 46<br/>
                <a href="tel:+78129875277">8 (812) 987-52-77</a>
            </div>

            <nav class="footer__nav">
                <ul>
                    <li><a href="#">Горящие туры</a></li>
                    <li><a href="#">Путевки выходного дня</a></li>
                    <li><a href="#">Путевки на 3 дня</a></li>
                </ul>
                <ul>
                    <li><a href="#">Путевки в Европу</a></li>
                    <li><a href="#">Как купить путевку?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </nav>
            <div class="footer__info">
                <a href="#" class="footer__android">
                    <img src="img/android.jpg" alt="" class="img-fluid">
                </a>
                <div class="footer__age"></div>
                <a class="footer__counter" href="#" target="_blank"><img src="img/counter.jpg" alt=""></a>
            </div>
        </div>
    </div>
</footer>
