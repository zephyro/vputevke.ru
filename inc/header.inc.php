<header class="header">
    <div class="header__top">
        <div class="header__container">
            <a href="#" class="header__toggle nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <a href="/" class="header__top_logo">
                <img src="img/logo.png" class="img-fluid" alt="">
            </a>
            <div class="header__time"><i class="fa fa-clock-o"></i><span>21:30</span></div>
            <span class="header__divider">|</span>
            <a href="#city_modal" class="header__city city_active btn_modal"><i class="fa fa-home"></i><span>Москва</span></a>
            <a href="#auth" class="header__login btn_modal"><i class="fa fa-user-o"></i><span>Вход / регистрация</span></a>
        </div>
    </div>
    <div class="header__menu">
        <div class="header__container">
            <a href="/" class="header__logo">
                <img src="img/logo.png" class="img-fluid" alt="">
            </a>
            <ul class="header__nav">
                <li><a href="#"><span>Горящие туры</span></a></li>
                <li><a href="#"><span>Путевки на 3 дня</span></a></li>
                <li><a href="#"><span>Путевки выходного дня</span></a></li>
                <li><a href="#"><span>Отдых в России</span></a></li>
                <li><a href="#"><span>Отдых с детьми</span></a></li>
            </ul>
        </div>
    </div>
</header>