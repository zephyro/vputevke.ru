
<!-- City -->
<div class="hide">
    <div class="modal" id="city_modal">
        <div class="modal__content">
            <div class="modal__city_active">Ваш город Москва?</div>
            <ul class="btn_group">
                <li><a href="#" class="btn">Нет изменить</a></li>
                <li><a href="#" class="btn btn_orange">Да, сохранить</a></li>
            </ul>
        </div>
    </div>
</div>


<!-- Auth -->

<div class="hide">
    <div class="authModal tabs" id="auth">
        <ul class="authModal__nav tabs_nav">
            <li class="active"><a href="#" data-target=".tab1">Вход</a></li>
            <li><a href="#" data-target=".tab2">Регистрация</a></li>
        </ul>
        <div class="authModal__content">
            <div class="tabs_item tab1 active">
                <form class="form">
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Имя\Email">
                    </div>
                    <div class="form_group">
                        <input type="password" class="form_control" name="password" placeholder="Пароль">
                    </div>
                    <button type="submit" class="btn btn_orange btn_send">Войти</button>
                </form>
                <div class="authModal__divider"><span>Войти через соц. сети</span></div>
                <ul class="social">
                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                    <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="tabs_item tab2">
                <form class="form">
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Логин">
                    </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="name" placeholder="Email">
                    </div>
                    <div class="form_group">
                        <input type="password" class="form_control" name="password" placeholder="Пароль">
                    </div>
                    <div class="form_group">
                        <input type="password" class="form_control" name="password" placeholder="Повторить пароль">
                    </div>
                    <button type="submit" class="btn btn_orange btn_send">Зарегистрироваться</button>
                </form>
            </div>
        </div>
    </div>
</div>
