<div class="searchBlock tabs">
    <ul class="searchBlock__nav tabs_nav">
        <li class="active"><a href="#" data-target=".tab1">Поиск туров</a></li>
        <li><a href="#" data-target=".tab2">Горящие туры</a></li>
    </ul>
    <div class="searchBlock__content">
        <div class="tabs_item tab1 active">
            <form class="search_form">
                <ul class="searchBlock__row">

                    <li>
                        <select class="form_select" name="direction">
                            <option value="Турция" selected>Куда</option>
                            <option value="Турция">Турция</option>
                            <option value="Испания">Испания</option>
                            <option value="Италия">Италия</option>
                            <option value="Египет">Египет</option>
                            <option value="Кипр">Кипр</option>
                            <option value="Чехия">Чехия</option>
                            <option value="Тайланд">Тайланд</option>
                            <option value="Франция">Франция</option>
                            <option value="Доминикана">Доминикана</option>
                            <option value="Мальдивы">Мальдивы</option>
                            <option value="Тунис">Тунис</option>
                            <option value="Мексика">Мексика</option>
                            <option value="Австрия">Австрия</option>
                            <option value="Болгария">Болгария</option>
                            <option value="Россия">Россия</option>
                            <option value="Финляндия">Финляндия</option>
                            <option value="Марокко">Марокко</option>
                            <option value="ОАЭ">ОАЭ</option>
                            <option value="Израиль">Израиль</option>
                        </select>
                    </li>

                    <li>
                        <input class="form_control filter-date" type="text" value="" name="date" placeholder="Дата вылета">
                    </li>

                    <li>
                        <div class="selection tourists">
                            <div class="selection_label">
                                <span class="selection_label_value">2 взрослых</span>
                                <b class="selection_label_button">▾</b>
                            </div>
                            <div class="selection_dropdown">
                                <div class="selection_dropdown_wrap">
                                    <div class="tourists_title tourists_adult">Взрослые</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="2" checked>
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="4">
                                                <span>4</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="tourists_title tourists_child">Дети от 3-х лет</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="0" checked>
                                                <span>нет</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="2">
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="tourists_title tourists_baby">Дети до 2-х лет</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="0" checked>
                                                <span>нет</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="2">
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="selection period">
                            <div class="selection_label">
                                <span class="selection_label_value">8 ночей</span>
                                <b class="selection_label_button">▾</b>
                            </div>
                            <div class="selection_dropdown">
                                <div class="selection_dropdown_wrap">
                                    <div class="period_info">Выбраны туры с 12.06.2018 на 9-15 ночей</div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form_number">
                                                <b class="form_number_change form_number_minus">
                                                    <i class="fa fa-minus-circle"></i>
                                                </b>
                                                <input type="text" class="form_number_value" value="8">
                                                <b class="form_number_change form_number_plus">
                                                    <i class="fa fa-plus-circle"></i>
                                                </b>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form_number">
                                                <b class="form_number_change form_number_minus">
                                                    <i class="fa fa-minus-circle"></i>
                                                </b>
                                                <input type="text" class="form_number_value" value="14">
                                                <b class="form_number_change form_number_plus">
                                                    <i class="fa fa-plus-circle"></i>
                                                </b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <select class="form_select" name="departure">
                            <option value="Москва" selected>из Москвы</option>
                            <option value="Волгоград">из Волгограда</option>
                            <option value="Санкт-Петербург">из Санкт-Петербурга</option>
                            <option value="Воронеж">из Воронежа</option>
                            <option value="Краснодар">из Краснодара</option>
                            <option value="Астрахань">из Астрахани</option>
                        </select>
                    </li>

                    <li>
                        <button class="btn btn_orange" type="submit"><i class="fa fa-search"></i><span>Искать</span></button>
                    </li>
                </ul>
            </form>
        </div>
        <div class="tabs_item tab2">
            <form class="search_form">
                <ul class="searchBlock__row">

                    <li>
                        <select class="form_select" name="direction">
                            <option value="Турция" selected>Куда</option>
                            <option value="Турция">Турция</option>
                            <option value="Испания">Испания</option>
                            <option value="Италия">Италия</option>
                            <option value="Египет">Египет</option>
                            <option value="Кипр">Кипр</option>
                            <option value="Чехия">Чехия</option>
                            <option value="Тайланд">Тайланд</option>
                            <option value="Франция">Франция</option>
                            <option value="Доминикана">Доминикана</option>
                            <option value="Мальдивы">Мальдивы</option>
                            <option value="Тунис">Тунис</option>
                            <option value="Мексика">Мексика</option>
                            <option value="Австрия">Австрия</option>
                            <option value="Болгария">Болгария</option>
                            <option value="Россия">Россия</option>
                            <option value="Финляндия">Финляндия</option>
                            <option value="Марокко">Марокко</option>
                            <option value="ОАЭ">ОАЭ</option>
                            <option value="Израиль">Израиль</option>
                        </select>
                    </li>

                    <li>
                        <input class="form_control filter-date" type="text" value="" name="date" placeholder="Дата вылета">
                    </li>

                    <li>
                        <div class="selection tourists">
                            <div class="selection_label">
                                <span class="selection_label_value">2 взрослых</span>
                                <b class="selection_label_button">▾</b>
                            </div>
                            <div class="selection_dropdown">
                                <div class="selection_dropdown_wrap">
                                    <div class="tourists_title tourists_adult">Взрослые</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="2" checked>
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="adult" value="4">
                                                <span>4</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="tourists_title tourists_child">Дети от 3-х лет</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="0" checked>
                                                <span>нет</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="2">
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="child" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="tourists_title tourists_baby">Дети до 2-х лет</div>
                                    <ul class="tourists_selection">
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="0" checked>
                                                <span>нет</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="1">
                                                <span>1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="2">
                                                <span>2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="tourists_radio">
                                                <input type="radio" name="baby" value="3">
                                                <span>3</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="selection period">
                            <div class="selection_label">
                                <span class="selection_label_value">8 ночей</span>
                                <b class="selection_label_button">▾</b>
                            </div>
                            <div class="selection_dropdown">
                                <div class="selection_dropdown_wrap">
                                    <div class="period_info">Выбраны туры с 12.06.2018 на 9-15 ночей</div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form_number">
                                                <b class="form_number_change form_number_minus">
                                                    <i class="fa fa-minus-circle"></i>
                                                </b>
                                                <input type="text" class="form_number_value" value="8">
                                                <b class="form_number_change form_number_plus">
                                                    <i class="fa fa-plus-circle"></i>
                                                </b>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="form_number">
                                                <b class="form_number_change form_number_minus">
                                                    <i class="fa fa-minus-circle"></i>
                                                </b>
                                                <input type="text" class="form_number_value" value="14">
                                                <b class="form_number_change form_number_plus">
                                                    <i class="fa fa-plus-circle"></i>
                                                </b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <select class="form_select" name="departure">
                            <option value="Москва" selected>из Москвы</option>
                            <option value="Волгоград">из Волгограда</option>
                            <option value="Санкт-Петербург">из Санкт-Петербурга</option>
                            <option value="Воронеж">из Воронежа</option>
                            <option value="Краснодар">из Краснодара</option>
                            <option value="Астрахань">из Астрахани</option>
                        </select>
                    </li>

                    <li>
                        <button class="btn btn_orange" type="submit"><i class="fa fa-search"></i><span>Искать</span></button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>