<div class="main__nav">

    <span class="main__nav_close nav-toggle"></span>
    <nav class="sidenav">
        <ul class="sidenav__one">
            <li><a href="#"><span>Путевки в Европу</span></a></li>
            <li><a href="#"><span>Отзывы путешественников</span></a></li>
            <li><a href="#"><span>Рейтинг отелей</span></a></li>
        </ul>
        <ul class="sidenav__two">
            <li><a href="#"><span>О нас</span></a></li>
            <li><a href="#"><span>Как купить путевку?</span></a></li>
        </ul>
    </nav>

</div>