<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="main__container">
                    <div class="main__nav">

                        <!-- Sidenav -->
                        <div class="main__nav">
                            <?php include('inc/sidenav.inc.php') ?>
                        </div>
                        <!-- -->

                    </div>
                    <section class="main__content">

                        <div class="main__search">

                            <!-- Search -->
                            <?php include('inc/search.inc.php') ?>
                            <!-- -->

                        </div>

                        <div class="main__wrap">

                            <div class="goods_block">
                                <h2>Горящие предложения</h2>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_01.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_03.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <div class="text-center">
                                    <a href="№" class="btn">Посмотреть все предложения</a>
                                </div>

                            </div>

                            <div class="divider"></div>

                            <h1>Путевки и горящие туры из Москвы</h1>
                            <p>Сегодня горящие путевки из Волгограда обладают огромной популярностью поэтому наш интернет-сайт по выбору путевок предлагает всем ознакомиться с информацией о данных спецпредложениях. Что такое горящие туры Современный рынок предлагает туры на различные необычные направления, правда стоимость туров отпугивает многих. Однако, имеется замечательная возможность поэкономить на отпуске, приобретя горящий тур.</p>
                            <p>Обратившись в проверенную туристическую фирму, за небольшие деньги вы получите высокий уровень обслуживания. Огромным преимуществом является шанс недорого получить комнату в замечательном пансионате. Продаются горящие путевки онлайн по различным причинам, однако обычно причина скидок в том, что турфирма бронирует все путевки заранее, до обращения потенциального клиента. Соответственно, часто возникает ситуация, когда место в лайнере и комнатка в гостинице уже оплачены, а клиент не нашелся. В таком случае, турфирма понижает стоимость на турне, и оно называется «горящим». Согласно путевке, скидка иногда превосходит 60% от первоначальной цены. Чем раньше день отъезда, тем дешевле может быть тур. Обычно, с момента покупки путевки из Волгограда до даты вылета остается одна неделя. </p>
                            <p>Как бронировать горящую путевку? Невзирая на значительное снижение стоимости, человек, выбравший горящий тур приобретает тот же перечень услуг, что и при обыкновенном туре. Выбрать путевки онлайн возможно на наиболее востребованные направления отдыха. Наш интернет-сайт оказывает помощь при выборе горящих туров, обрабатывая информацию ведущих туроператоров. Соответственно, вы не пропустите наилучшие предложение по самой выгодной цене. </p>
                            <p>Благодаря возможности покупать турне через Internet, вы значительно бережете личное время. Отбор осуществляется впоследствии того, как вы пошагово отметите условия, которым должна отвечать ваша горящая путевка. На этом этапе клиент указывает направление (государство либо конкретный курорт), стол в гостинице, число отдыхающих и прочее. Установив фильтр по стоимости, вы будете просматривать лишь те путевки, которые соответствуют вашему бюджету. Теперь купить путевку онлайн возможно выгодно и оперативно. Среди большого число путевок, большая часть людей покупают именно горящие турне. Мы, в ответ, сделаем для вас процесс выбора тура очень удобным.</p>

                            <div class="divider"></div>

                            <div class="hews_block">

                                <h2>Новости</h2>


                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                        <div class="news__text">Quisque luctus dui vitae odio varius pulvinar. Sed feugiat, nunc eget semper volutpat, mauris arcu mattis metus, interdum tempor velit lorem sodales ligula. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Fusce interdum eleifend nisl, sed venenatis dui eleifend nec.</h4>
                                        <div class="news__text">Nam id risus vitae enim facilisis fringilla. Mauris vitae leo tellus. Quisque nec erat neque. Quisque eu sagittis tellus, congue consectetur magna. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Donec vulputate neque lacus, id mattis turpis accumsan id.</h4>
                                        <div class="news__text">Curabitur in erat vulputate, eleifend tortor nec, interdum tellus. Phasellus dignissim pulvinar nulla, in congue ipsum laoreet a. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/04.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                        <div class="news__text">Quisque luctus dui vitae odio varius pulvinar. Sed feugiat, nunc eget semper volutpat, mauris arcu mattis metus, interdum tempor velit lorem sodales ligula. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Fusce interdum eleifend nisl, sed venenatis dui eleifend nec.</h4>
                                        <div class="news__text">Nam id risus vitae enim facilisis fringilla. Mauris vitae leo tellus. Quisque nec erat neque. Quisque eu sagittis tellus, congue consectetur magna. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <a href="#" class="news">
                                    <div class="news__image">
                                        <img src="images/02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="news__content">
                                        <h4>Donec vulputate neque lacus, id mattis turpis accumsan id.</h4>
                                        <div class="news__text">Curabitur in erat vulputate, eleifend tortor nec, interdum tellus. Phasellus dignissim pulvinar nulla, in congue ipsum laoreet a. </div>
                                        <div class="news__tag">
                                            <span>Сегодня, 08:53</span>
                                            <span>Просмотров: 117</span>
                                        </div>
                                    </div>
                                </a>

                                <div class="text-center">
                                    <a href="№" class="btn">Посмотреть все новости</a>
                                </div>

                            </div>

                        </div>

                    </section>
                    <aside class="main__sidebar">

                        <!-- Countries -->
                        <?php include('inc/countries.inc.php') ?>
                        <!-- -->

                    </aside>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->


    </body>
</html>
