// --- Top Nav

$(function() {
    var pull = $('.nav-toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
    });
});

// --- Filter
$(function() {

    $('.filter-toggle').on('click', function(e) {
        e.preventDefault();
        $('.main__sidebar').toggleClass('open');
    });
});



// --- Modal

$(".btn_modal").fancybox({
    'padding' : 0
});


// --- Form Select

$('.form_select').selectric({
    maxHeight: 200,
    disableOnMobile: false,
    nativeOnMobile: false,
    responsive: true
});

// --- Datetimepicker
jQuery(document).ready(function () {
    'use strict';

    jQuery.datetimepicker.setLocale('ru');


    jQuery('.filter-date').datetimepicker({
        format:'d.m',
        lang:'ru',
        timepicker:false
    });
});



var hotel = new Swiper('.hotel_slider_body', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


$('.hotel_slider_thumbs li a').on('click touchstart', function(event){
    event.preventDefault();

    var index = $(this).attr("data-target");
    $(this).closest('.hotel_slider_thumbs').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    console.log(index);
    hotel.slideTo(index);
});



$(document).ready(function(){

    $('.selection_label').on('click touchstart', function(event){
        $(this).closest('.selection').toggleClass('open');
    });

    $(document).on('click touchstart', function(event) {
        if ($(event.target).closest('.selection').length){
            return;
        }else{
            $('.selection').removeClass('open');
        }
        event.stopPropagation();
    });
});

// Input number

$('.form_number_minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.form_number_plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});
