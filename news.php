<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <div class="main">
                <div class="main__container">
                    <div class="main__nav">

                        <!-- Sidenav -->
                        <div class="main__nav">
                            <?php include('inc/sidenav.inc.php') ?>
                        </div>
                        <!-- -->

                    </div>
                    <section class="main__content">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Новости</a></li>
                            <li><span>Пляжный отдых</span></li>
                        </ul>


                        <div class="main__wrap">

                            <div class="post">
                                <div class="post__title">
                                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id tincidunt ligula.</h1>
                                </div>
                                <div class="post__image">
                                    <img src="images/news.jpg" class="img-fluid" alt="">
                                </div>

                                <div class="post__content">
                                    <p>Aliquam pharetra tincidunt metus non consequat. Aliquam cursus luctus felis sollicitudin eleifend. Proin elit turpis, ultricies sed turpis sit amet, volutpat semper ligula. Pellentesque placerat posuere risus eget tristique. Morbi consectetur tristique nisi et tristique. Nullam sit amet mi sit amet nibh sollicitudin sollicitudin nec ac purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <p>Fusce in tortor cursus, dapibus magna in, sodales lorem. Donec scelerisque sed neque a fringilla. Cras a ante risus. Phasellus ullamcorper dolor at pulvinar dictum. Ut iaculis, felis ut hendrerit sodales, dolor elit tempus mauris, a gravida magna turpis et nunc. Pellentesque sed justo ut libero dictum condimentum. Cras et malesuada libero. Sed et porttitor turpis. Nam nec diam sed velit malesuada accumsan. Suspendisse eu rhoncus nibh. Suspendisse viverra euismod purus id malesuada. In leo mi, ornare nec leo ut, rutrum malesuada tellus. Aliquam sit amet mauris sed nisi cursus mollis at sit amet felis.</p>
                                    <p>Aliquam pharetra tincidunt metus non consequat. Aliquam cursus luctus felis sollicitudin eleifend. Proin elit turpis, ultricies sed turpis sit amet, volutpat semper ligula. Pellentesque placerat posuere risus eget tristique. Morbi consectetur tristique nisi et tristique. Nullam sit amet mi sit amet nibh sollicitudin sollicitudin nec ac purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>

                                <ul class="post__tags">
                                    <li><a href="#"><i class="fa fa-tag"></i> Отдых</a></li>
                                    <li><i class="fa fa-calendar"></i> 20.04.2018</li>
                                    <li><i class="fa fa-eye"></i> 28</li>
                                </ul>
                            </div>

                        </div>

                    </section>
                    <aside class="main__sidebar">

                        <!-- Countries -->
                        <?php include('inc/countries.inc.php') ?>
                        <!-- -->

                    </aside>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
