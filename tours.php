<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->
                        <div class="main">
                <div class="main__container">

                    <!-- Sidenav -->
                    <div class="main__nav">
                        <?php include('inc/sidenav.inc.php') ?>
                    </div>
                    <!-- -->

                    <section class="main__content">

                        <div class="main__search">

                            <!-- Search -->
                            <?php include('inc/search.inc.php') ?>
                            <!-- -->

                        </div>

                        <div class="main__wrap">

                            <div class="goods_block">

                                <div class="goods_filter">
                                    <div class="goods_sort">
                                        <select class="form_select">
                                            <option value="">Сортировать</option>
                                            <option value="">по цене</option>
                                            <option value="">по популярности</option>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span class="btn_filter filter-toggle">
                                        <i class="fa fa-sliders" aria-hidden="true"></i>
                                    </span>
                                </div>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_01.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_03.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_01.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_03.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                                <a class="goods" href="#">
                                    <div class="goods__image" style="background-image: url('images/hotel_02.jpg');"></div>
                                    <div class="goods__content">
                                        <div class="goods__rate">
                                            <div class="goods__rate_star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="goods__rate_rating rating_good">4.7</div>
                                        </div>
                                        <div class="goods__content_left">

                                            <div class="goods__name">Sveltos Hotel</div>

                                            <div class="goods__place">Кипр, Ларнака</div>

                                        </div>
                                        <div class="goods__content_right">
                                            <div class="goods__params"><span><i class="fa fa-calendar"></i> c 8 июня</span> <span>|</span> <span><i class="fa fa-moon-o"></i> на 8 ночей</span></div>
                                            <div class="goods__price">
                                                <div class="goods__price_old"><strong>92500</strong> р.</div>
                                                <div class="goods__price_new">от <strong>72500</strong> р.</div>
                                            </div>
                                            <span class="btn btn_orange">подробнее</span>
                                        </div>
                                    </div>
                                </a>

                            </div>

                        </div>

                    </section>
                    <aside class="main__sidebar">
                        <div class="main__sidebar_layout filter-toggle"></div>
                        <!-- Filters -->
                        <?php include('inc/filter.inc.php') ?>
                        <!-- -->

                    </aside>
                </div>
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script>
            var $range = $(".filters__price_slider");

            $range.ionRangeSlider({
                type: "double",
                min: 5000,
                max: 200000,
                from: 8000,
                to: 150000,
                step: 5000
            });
        </script>

    </body>
</html>
